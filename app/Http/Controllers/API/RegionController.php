 <?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Region;
class RegionController extends Controller
{
    
    public function showAll(){

        $regions = Region::with('districts')->get();

        return response()->json(['results'=>$regions,'status'=>'success']);

    }

    public function show($id){

        $region=Region::with('districts.schools')->select('id','name')->find($id);
        return response()->json(['results'=>$region,'status'=>'success']);

    }



    // public function nameFormat($name){

    //     $upper = strtoupper($name[0]);
    //     $lower=strtolower(substr($name,1));
    //     return "{$upper}{$lower}";
    // }
}
