<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'schools';

    /**
     * Run the migrations.
     * @table schools
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable()->default(null);
            $table->string('postal_address', 45)->nullable()->default(null);
            $table->unsignedInteger('district_id');
            $table->string('phone_number', 45)->nullable()->default(null);
            $table->string('email', 45)->nullable()->default(null);
            $table->string('fax', 45)->nullable()->default(null);
            $table->string('website', 45)->nullable()->default(null);
            $table->integer('fee')->nullable()->default(null);
            $table->string('logo');
            $table->string('image');
            $table->string('description')->nullable()->default(null);
            $table->string('requirement')->nullable()->default(null);
            $table->enum('accomodation_type', ['day', 'bording', 'both'])->nullable()->default(null);
            $table->enum('accepted_gender', ['girls', 'boys', 'both'])->nullable()->default(null);

            $table->index(["district_id"], 'fk_schools_districts1_idx');
            $table->nullableTimestamps();


            $table->foreign('district_id', 'fk_schools_districts1_idx')
                ->references('id')->on('districts')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
