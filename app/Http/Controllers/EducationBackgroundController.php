<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EducationBackground;

class EducationBackgroundController extends Controller
{
    //
    public function addRecord(Request $request){
         $data = new EducationBackground();

         $data->completion_year =$request->compyr;
         $data->examination_number =$request->indexnumber;
         $data->division =$request->division;
         $data->result_file =$request->result_file;
         $data->prev_school_name =$request->prevschool;
         $data->user_id =4;

         $data->save();
    }
}
