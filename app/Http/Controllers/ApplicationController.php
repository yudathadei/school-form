<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Application;

class ApplicationController extends Controller
{
    //
    public function create(Request $request){

        $data = new Application();

        $data->status ="selected";
        $data->user_id = 4;
        $data->education_level = 1;
        $data->grade = $request->app_grade;
        $data->category = $request->app_category;

        $data->save();
    }
}
