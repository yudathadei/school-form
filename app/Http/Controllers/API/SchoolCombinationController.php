<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\SchoolCombination;

class SchoolCombinationController extends Controller
{
    public function create(Request $request){
        
        $data = new SchoolCombination();

        $data->combination_id = $request->combination_id;
        $data->school_id = $request->school_id;

        if($data->save()) return response()->json(['message'=>'schoolcombination created']);  
     }

    
     public function show($id){

        $data = SchoolCombination::with('combination')->find($id);

        return response()->json(['results'=>$data]);


     }
    
}
