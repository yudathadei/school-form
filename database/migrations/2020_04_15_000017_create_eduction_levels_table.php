<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEductionLevelsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'eduction_levels';

    /**
     * Run the migrations.
     * @table eduction_levels
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->enum('level', ['o-level', 'a-level'])->nullable()->default(null);
            $table->string('grade', 45)->nullable()->default(null);
            $table->enum('category', ['sience', 'art', 'busines'])->nullable()->default(null);
            $table->unsignedInteger('application_id');

            $table->index(["application_id"], 'fk_eduction_levels_applications1_idx');


            $table->foreign('application_id', 'fk_eduction_levels_applications1_idx')
                ->references('id')->on('applications')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
