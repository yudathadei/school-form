<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Interview;

class InterviewController extends Controller
{
    //
    public function create(Request $request){


        $data = new Interview();

        $data->date = $request->ent_date ;
        $data->venue = $request->ent_venue;
        $data->time = $request->ent_time;
        $data->notice = $request->ent_notice;
        $data->application_deadline = $request->app_deadline;
        $data->application_fee = $request->app_fee;
        $data->school_id =1;

        $data->save();
    }
}
