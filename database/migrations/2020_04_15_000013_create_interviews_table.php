<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterviewsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'interviews';

    /**
     * Run the migrations.
     * @table interviews
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->date('date')->nullable()->default(null);
            $table->string('venue', 200)->nullable()->default(null);
            $table->string('time', 45)->nullable()->default(null);
            $table->date('application_deadline')->nullable()->default(null);
            $table->integer('application_fee')->nullable()->default(null);
            $table->string('notice')->nullable()->default(null);
            $table->unsignedInteger('school_id');

            $table->index(["school_id"], 'fk_interviews_schools1_idx');


            $table->foreign('school_id', 'fk_interviews_schools1_idx')
                ->references('id')->on('schools')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
