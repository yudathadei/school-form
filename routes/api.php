<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('users','API\UserController@showAll');

Route::get('users/{id}','API\UserController@show');

Route::get('test','API\UserTypeController@test');

Route::post('users','API\UserController@create');

Route::get('userdelete/{id}','API\UserController@delete');

Route::put('userupdate/{id}','API\UserController@update');


Route::post('application','API\ApplicationController@create');

Route::get('application/{id}','API\ApplicationController@show');

Route::get('application','API\ApplicationController@showAll');

Route::put('application/update/{id}','API\ApplicationController@update');

Route::get('application/delete/{id}','API\ApplicationController@delete');


Route::get('regions','API\RegionController@showAll');

Route::get('regions/{region}','API\RegionController@show');


Route::post('school','API\SchoolController@create');

Route::get('school','API\SchoolController@showAll');

Route::get('school/{id}','API\SchoolController@show');

Route::get('school/delete/{id}','API\SchoolController@delete');

Route::put('school/update/{id}','API\SchoolController@update');


Route::post('document','API\DocumentController@create');

Route::get('document','API\DocumentController@showAll');

Route::get('document/{id}','API\DocumentController@show');

Route::put('document/update/{id}','API\DocumentController@update');

Route::get('document/delete/{id}','API\DocumentController@delete');


Route::post('payment','API\PaymentController@create');

Route::get('payment','API\PaymentController@showAll');

Route::get('payment/{id}','API\PaymentController@show');

Route::put('payment/{id}','API\PaymentController@update');

Route::get('payment/delete/{id}','API\PaymentController@delete');


Route::post('combination','API\CombinationController@create');

Route::get('combination','API\CombinationController@show');


Route::post('applicationcombination','API\ApplicationCombinationController@create');

Route::get('applicationcombination','API\ApplicationCombinationController@showAll');

Route::get('applicationcombination/{id}','API\ApplicationCombinationController@show');

Route::put('applicationcombination/update/{id}','API\ApplicationCombinationController@update');

Route::get('applicationcombination/delete/{id}','API\ApplicationCombinationController@delete');


Route::post('schoolcombination','API\SchoolCombinationController@create');

Route::get('schoolcombination/{id}','API\SchoolCombinationController@show');

Route::post('parentstudent','API\ParentStudentController@create');

Route::get('parentstudent','API\ParentStudentController@showAll');

Route::get('parentstudent/{id}','API\ParentStudentController@show');


Route::get('district/{id}','API\DistrictController@show');












