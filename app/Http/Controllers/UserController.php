<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class UserController extends Controller
{
    public function create(Request $request){

        $user = new User(); 
        $user->fname = $request->fname; 
        $user->mname = $request->mname; 
        $user->lname = $request->lname;
        $user->phone_number = $request->phone_number;
        $user->gender  = $request->gender;
        $user->email  = $request->email;
        $user->birth_date = $request->birth_date;
        $user->user_type_id = $request->user_type_id; 
        $user->password  = $request->password;
    
        if($request->user_type_id = 2){
            return response()->json(['result'=>$user,'message'=>'User must be parent']);
        }

        if($request->user_type_id = 3){
            return response()->json(['result'=>$user,'message'=>'User must be administrator']);
        }
        

        if($user->save()) return response()->json(['result'=>$user,'message'=>'User created succesfuly']);

        else return response()->json(['message'=>'Failed to create user']); 
 
    }

    public function openForm(){

        return view('form');
    }
    public function addStudent(Request $request){
        $user = new User();

        $user->fname = $request->Sfname; 
        $user->mname = "Ali"; 
        $user->lname = $request->Ssname;
        $user->phone_number = $request->Stell;
        $user->gender  = $request->Sgender;
        $user->email  = $request->Semail;
        $user->birth_date = $request->Sbirthdate;
        $user->user_type_id = 1;
        $user->religion = $request->Sreligion;
        $user->address = $request->Saddress;
        $user->birth_place = $request->Sbirthplace;
        $user->age = $request->Sage;

        $user->save();

    }

    public function addGuardian(Request $request){

        $guardian1 = new User();

        $guardian1->fname = $request->g1fname;
        $guardian1->lname = $request->g1sname;
        $guardian1->user_type_id = 2;
        $guardian1->email = $request->g1email;
        $guardian1->phone_number = $request->g1tell;
        $guardian1->occupation = $request->g1occupation;
        $guardian1->address = $request->g1address;
        $guardian1->relationship = $request->g1rlship;

        $guardian1->save();

        $guardian2 = new User();

        $guardian2->fname = $request->g2fname;
        $guardian2->lname = $request->g2sname;
        $guardian2->user_type_id = 2;
        $guardian2->email = $request->g2email;
        $guardian2->phone_number = $request->g2tell;
        $guardian2->occupation = $request->g2occupation;
        $guardian2->address = $request->g2address;
        $guardian2->relationship = $request->g2rlship;

        $guardian2->save();

    }
    public function fillForm(Request $request){     

        $education_background = app('App\Http\Controllers\EducationBackgroundController');
        $education_background->addRecord($request);
        $application = app('App\Http\Controllers\ApplicationController');
        $application->create($request);
        $this->addStudent($request);
        $this->addGuardian($request);

        return redirect('/');
    }


}
