<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Combination extends Model
{
    public function applicationCombinations(){

        return $this->hasMany('App\ApplicationCombination');
    }

    public function schoolCombinations(){

        return $this->hasMany('SchoolCombination');
    }
}
