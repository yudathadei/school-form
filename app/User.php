<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable

{
    protected $fillable = ['fname','mname'];
    // use Notifiable;

    // /**
    //  * The attributes that are mass assignable.
    //  *
    //  * @var array
    //  */
    // protected $fillable = [
    //     'name', 'email', 'password',
    // ];

    // /**
    //  * The attributes that should be hidden for arrays.
    //  *
    //  * @var array
    //  */
    // protected $hidden = [
    //     'password', 'remember_token',
    // ];

    // /**
    //  * The attributes that should be cast to native types.
    //  *
    //  * @var array
    //  */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];
    public function applications(){

        return $this->hasMany('App\Application');
    }
    // public function parentStudents(){

    //     return $this->hasMany('App\ParentStudent');
    // }

    public function usertype(){

        return $this->belongsTo('App\UserType','user_type_id');
    }

    public function educationBackgrounds(){

        return $this->hasMany('App\EducationBackground');
    }

}
