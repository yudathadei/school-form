<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="{{asset('bootstrap-4.4.1-dist/css/bootstrap.min.css')}}" >
<link href="{{asset('https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css')}}" rel="stylesheet">
<link href="{{asset('https://fonts.googleapis.com/css?family=Lato&display=swap')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('path/to/font-awesome/css/font-awesome.min.css')}}">
<link href="{{ asset('css/sf.css') }}" rel="stylesheet" type="text/css" >
</head>
<body>
<div class="container-fluid school-header">
    <div class="container ">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="/home">
                <img src="{{asset('images/logo.svg')}}"  class="d-inline-block align-top logo-sm" alt="">
            </a>
            <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <div class="srch-bar-list">
                    <ul class="navbar-nav mr-auto">
                        <div class="srch-form-wrapper">
                            <form class="form-inline my-2 my-lg-0">
                                <input class="form-control mr-sm-2 srch-bar" type="search" placeholder="Search" aria-label="Search">
                                <button class="btn btn-color" type="submit">Search</button>
                            </form>
                        </div>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Contact Us</a>
                        </li>
                        <li class="nav-item">
                            <button  type="submit" class="btn btn-color ">Log in</button>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>

<div class="container ">
    <div class="row sch">
        <p class="sch-name"> {{$school->name}}</p>
        <div class="sch-img">
            <img src="{{asset('images/school.jpg')}}" alt="">
        </div>
    </div>
    <div class="row sch-table">
        <div class="table-wrapper">
            <table class="table table-bordered ">
                <thead class="btn-color">
                    <tr>
                    <th colspan="2" class="bld-center"> Form Admission details</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <td> Application Deadline :</td>
                    <td class="bld-center">{{$interview->application_deadline}} </td>
                    </tr>
                    <tr>
                    <td>Interview Date :</td>
                    <td class="bld-center">{{$interview->date}}</td>
                    </tr>
                    <tr>
                    <td>Interview Time :</td>
                    <td class="bld-center">{{$interview->time}}</td>
                    </tr>
                    <tr>
                    <td style="background-color:white !important"></td>
                    <td style="background-color:white !important"></td>
                    </tr>
                    <tr>
                    <tr>
                        <td class="bld-center" colspan="2">Application Fee: Tsh {{$interview->application_fee}}/=</td>
                    </tr>
                    <tr>
                    <td colspan="2" class="tb-footer">
                        <a href="/form">
                            <button  type="submit" class="btn btn-color ">Apply now</button>
                        </a>    
                    </td>
                    </tr>
                </tbody>
            </table>
    </div>
    </div>
    <div class="row sch-detail">
        <div class=" row sch-history">
            <h4>History</h4>
            <p class="sch-detail-sec">
                {{$school->description}}
            </p>
        </div>
        <div class="row sch-reqrmnt">
            <h4>Admission Requirements</h4>
            <div class="sch-detail-sec">
                <ul>
                    <li>Parent/School contract form dully signed.</li>
                    <li>Parent/School contract form dully signed.</li>
                    <li>Parent/School contract form dully signed.</li>
                    <li>Parent/School contract form dully signed.</li>
                    <li>Parent/School contract form dully signed.</li>

                </ul>
            </div>
        </div>
        <div class="row sch-contact">
            <h4>Contacts</h4>
            <div class="sch-detail-sec">
                <div class="sch-contact">
                    <img  src="{{asset('images/phone.png')}}" alt="">
                    <span>{{$school->phone_number}}</span>
                </div>
                <div class="sch-contact">
                    <img src="{{asset('images/box.png')}}" alt="">
                    <span>{{$school->postal_address}}</span>
                </div>
                <div class="sch-contact">
                    <img src="{{asset('images/mail.png')}}" alt="">
                    <span>{{$school->email}}</span>
                </div>
                <div class="sch-contact">
                    <img src="{{asset('images/web.png')}}" alt="">
                    <span><a href="/{{$school->website}}">{{$school->website}}</a></span>
                </div>
            </div>
        </div>
        <div class="row sch-note">
            <h4>NOTE</h4>
            <p class="sch-detail-sec">
                {{$interview->notice}}
            </p>
        </div>
    </div>

</div>

<div class="container-fluid school-footer">
    
</div>


<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="{{URL::asset('js/sf.js')}}"></script>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    
</body>
</html>
