<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User; 
use Validator; 


class UserController extends Controller
{


    public function showAll(){

        $user = User::all();

        return response()->json(['results'=>$user]);


    }

    public function show($id){

        $user = User::with('applications.documents','applications.payment','usertype','applications.applicationCombinations.combination')->find($id);

        return response()->json(['results'=>$user]);
    }

    public function create(Request $request){

        $user = new User(); 
        $user->fname = $request->fname; 
        $user->mname = $request->mname; 
        $user->lname = $request->lname;
        $user->phone_number = $request->phone_number;
        $user->gender  = $request->gender;
        $user->email  = $request->email;
        $user->birth_date = $request->birth_date;
        $user->user_type_id = $request->user_type_id; 
        $user->password  = $request->password;

        if($request->user_type_id = 2){
            return response()->json(['result'=>$user,'message'=>'User must be parent']);
        }

        if($request->user_type_id = 3){
            return response()->json(['result'=>$user,'message'=>'User must be administrator']);
        }
        

        if($user->save()) return response()->json(['result'=>$user,'message'=>'User created succesfuly']);

        else return response()->json(['message'=>'Failed to create user']); 

    }

    public function delete($id){

        $user = User::find($id);

        if($user->delete()) return response()->json(['message'=>'user deleted']);

        else return response()->json(['error'=> $user->errors()]);

    }

    public function update(Request $request,$id){

        $user = User::find($id);
    
        $user->fname = $request->has('fname')?$request->fname:$user->fname; 
        $user->mname = $request->has('mnane')?$request->mname:$user->mname; 
        $user->lname = $request->has('lname')?$request->lname:$user->lname;
        $user->phone_number = $request->has('phone_number')?$request->phone_number:$user->phone_number;
        $user->gender  = $request->has('gender')?$request->gender:$user->gender;
        $user->email  = $request->has('email')?$request->email:$user->email;
        $user->birth_date = $request->has('birth_date')?$request->birth_date:$user->birth_date;
        $user->user_type_id = $request->has('user_type_id')? $request->user_type_id:$user->user_type_id; 
        $user->password  = $request->has('password')? $request->password:$user->password;

        if($user->save()) return response()->json(['message','user updated']);
        else return response()->json(['error',$user->erros()]);


    }


}
