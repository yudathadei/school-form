<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Combination;

class CombinationController extends Controller
{
    public function create(Request $request){

        $combination = new Combination();

        $combination->name = $request->name;

        if($combination->save()) return response()->json(['message','combination created']);
    }

    public function show(){

        $combination = Combination::all();

        return response()->json(['results'=>$combination]);
    }
}
