<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'users';

    /**
     * Run the migrations.
     * @table users
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('fname', 45)->nullable()->default(null);
            $table->string('lname', 45)->nullable()->default(null);
            $table->string('mname', 45)->nullable()->default(null);
            $table->string('phone_number', 45)->nullable()->default(null);
            $table->date('birth_date')->nullable()->default(null);
            $table->string('password', 60)->nullable()->default(null);
            $table->unsignedInteger('user_type_id');
            $table->enum('gender', ['male', 'female'])->nullable()->default(null);
            $table->string('email', 60)->nullable()->default(null);
            $table->integer('age')->nullable()->default(null);
            $table->string('religion')->nullable()->default(null);
            $table->string('birth_place')->nullable()->default(null);
            $table->string('occupation')->nullable()->default(null);
            $table->string('relationship')->nullable()->default(null);
            $table->string('address', 250)->nullable()->default(null);

            $table->index(["user_type_id"], 'fk_users_user_types1_idx');
            $table->nullableTimestamps();


            $table->foreign('user_type_id', 'fk_users_user_types1_idx')
                ->references('id')->on('user_types')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
