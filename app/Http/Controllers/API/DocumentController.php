<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Document;

class DocumentController extends Controller
{
    public function create(Request $request){

        $document = new Document();

        $document->name = $request->name;
        $document->file = $request->file;
        $document->application_id = $request->application_id;

        if($document->save()) return response()->json(['message'=>'document added']);
        else return response()->json(['error'=>'Failed to add document']);
    }
    public function update(Request $request, $id){

        $document = Document::find($id);

        $document->name = $request->has('name')?$request->name :$document->name;
        $document->file = $request->has('file')?$request->file: $document->file;
        $document->application_id = $request->has('application_id')? $request->application_id:$document->application_id;

        if($document->save()) return $this->show($id);
        // if($document->save()) return response()->json(['message'=>'document updated']);
        else return response()->json(['error'=>'Failed to update document']);
    }

    public function showAll(){

        $document = Document::all();

        return response()->json(['results'=>$document]);
    }   
    public function show($id){

        $document = Document::find($id);

        return response()->json(['results'=>$document]);
    }

    public function delete($id){

        $document = Document::find($id);

        if($document->delete()) return response()->json(['message'=>'document deleted']);
    }
}
