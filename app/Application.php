<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    public function applicationCombinations(){

        return $this->hasMany('App\ApplicationCombination');
    }

    public function documents(){

        return $this ->hasMany('App\Document');
    }

    public function payment(){

        return $this->hasOne('App\Payment');
    }

    public function user(){

        return $this->belongsTo('App\User');                                                                    
    }
    
}
