<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // User::truncate();

        $faker = \Faker\Factory::create();

        for($i=0;$i<20;$i++){

            User::create([
                'fname'=> $faker->firstNameMale,
                'mname'=> $faker->firstNameFemale,
                'lname'=> $faker->lastName,
                'user_type_id'=> $faker->randomDigit,
                'birth_date'=> $faker->date($format = 'Y-m-d', $max = 'now'),
                'phone_number'=> $faker->phoneNumber
            ]);
        }
    }
}
