<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApplicationCombination;

class ApplicationCombinationController extends Controller
{
    public function create(Request $request){

        $appc = new ApplicationCombination();

        $appc->priority = $request->priority;
        $appc->application_id = $request->application_id;
        $appc->combination_id = $request->combination_id;

        if($appc->save()) return response()->json(['message'=>'application_combination created']);
        else response()->json(['message'=>'application_combination not created']);
    }

    public function update(Request $request,$id){

        $appc = ApplicationCombination::find($id);

        $appc->priority = $request->has('priority')?$request->priority:$appc->priority;
        $appc->application_id = $request->has('application_id')?$request->application_id:$appc->application_id;
        $appc->combination_id = $request->has('combination_id')?$request->combination_id:$appc->combination_id;

        if($appc->save()) return response()->json(['message'=>'application_combination updated']);
        else response()->json(['message'=>'application_combination not updated']);
    }

    public function showAll(){

        $appc = ApplicationCombination::all();

        return response()->json(['results'=>$appc]);
    }

    public function show($id){

        $appc = ApplicationCombination::find($id);

        return response()->json(['results'=>$appc]);
    }

    public function delete($id){

        $appc = ApplicationCombination::find($id);

        if($appc->save()) return response()->json(['message'=>'application combination deleted']);
    }


}
