<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public function application(){

        return $this->belongsTo('App\Application','application_id');
    }
}
