<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Application;
use App\User;

class ApplicationController extends Controller
{
    public function create(Request $request){

        $application = new Application();

        $application->status = $request->status;
        $application->education_level = $request->education_level;
        $application->user_id = $request->user_id;

        if($application->save()) return response()->json(['message'=>'application created succefuly']);
        else return response()->json(['error'=>$application->errors()]);

    }

    public function show($id){

        $application = Application::with('user')->find($id)->payment;
        
        return response()->json(['results'=>$application]);
    }

    public function showAll(){

        $application = Application::with('user')->get();
        
        return response()->json(['results'=>$application]);

    }

    public function update(Request $request,$id){

        $application = Application::find($id);

        $application->status = $request->has('status')?$request->status:$application->status;
        $application->education_level = $request->has('education_level')?$request->education_level:$application->education_level;
        $application->user_id = $request->has('user_id')?$request->user_id:$application->user_id;

        if($application->save()) return $this->show($id);

        else return response()->json(['error'=>'failed to update']);
    }

    public function delete($id){

        $application = Application::find($id);

        if($application->delete()) return response()->json(['message'=>'Application deleted']);
        else return response()->json(['error'=>'some error']);
    }
}
