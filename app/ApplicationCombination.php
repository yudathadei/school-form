<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationCombination extends Model
{
    public function application(){

        return $this->belongsTo('App\Application');
    }

    public function combination(){
        
        return $this->belongsTo('App\Combination');
    }
}
