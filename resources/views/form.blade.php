@extends('layouts.mainlayout')
@section('content')
    <div class="container-fluid school-header">
    <div class="container ">
      <nav class="navbar navbar-expand-lg navbar-light ">
        <div class="row navcontent-wrapper">
          <div class="col-md-1">
          <a class="navbar-brand" href="/home">
                  <img src="images/logo.svg"  class="d-inline-block align-top logo-sm" alt="">
              </a>
              <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
              </button>
          </div>
          <div class="col-md-8">
                  <!--progress bar-->
            <div class="row progress-wrapper">
              <div class="col-12 col-lg-8 ml-auto mr-auto mb-4 ">
                <div class="multisteps-form__progress" style="min-width: 700px !important"> 
                  <button class="multisteps-form__progress-btn js-active" type="button" title="User Info">Personal Details</button>
                  <button class="multisteps-form__progress-btn" type="button" title="Address">Educational Details</button>
                  <button class="multisteps-form__progress-btn" type="button" title="Order Info">Parents Details</button>
                  <button class="multisteps-form__progress-btn" type="button" title="Comments">Confirm Details </button>
                  <button class="multisteps-form__progress-btn" type="button" title="Comments">Payments      </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </nav>
    </div>
</div>
<div class="row" style="min-height:40px"></div>
    <div class="container " >
      <!--multisteps-form-->
      <div class=" multisteps-form overflow-hidden">
        <!--form panels-->
        <div class="row">
          <div class="col-12 col-lg-8 m-auto">
            <form  method="POST" action="fillform" class="multisteps-form__form">
            @csrf
              <!--single form personal-info-panel-->
              <div class="multisteps-form__panel shadow p-4 rounded bg-white js-active" data-animation="scaleIn">
                <h3 class="multisteps-form__title">Personal Information</h3>
                <div class="multisteps-form__content">
                  <div class="row form-content">
                      <div class="form-row wid-100">
                        <div class="form-group col-md-6">
                          <label for="inputEmail4">First Name</label>
                          <input name="Sfname" type="text" class="form-control" id="Sfname">
                        </div>
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">Second Name</label>
                          <input name="Ssname" type="text" class="form-control" id="Ssname">
                        </div>
                      </div>
                      <div class="form-group wid-100">
                        <label for="inputAddress">Email</label>
                        <input name="Semail" type="email" class="form-control" id="Semail" placeholder="1234 Main St">
                      </div>
                      <div class="form-row wid-100">
                        <div class="form-group col-md-4">
                          <label for="inputCity">Dirt of Birth</label>
                          <input name="Sbirthdate" type="date" class="form-control" id="Sbirthdate">
                        </div>
                        <div class="form-group col-md-4">
                          <label for="inputState">Gender</label>
                          <select name="Sgender" id="Sgender" class="form-control">
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                          </select>
                        </div>
                        <div class="form-group col-md-4">
                          <label for="inputZip">Age</label>
                          <input name="Sage" type="number" class="form-control" id="Sage">
                        </div>
                      </div>
                      <div class="form-row wid-100">
                        <div class="form-group col-md-6">
                          <label for="inputEmail4">Nationality</label>
                          <input name="Snationality" type="text" class="form-control" id="Snationality">
                        </div>
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">Place of Birth</label>
                          <input name="Sbirthplace" type="text" class="form-control" id="Splacebirth">
                        </div>
                      </div>
                      <div class="form-row wid-100">
                        <div class="form-group col-md-4">
                          <label for="inputCity">Telephone</label>
                          <input name="Stell" type="number" class="form-control" id="Stell">
                        </div>

                        <div class="form-group col-md-4">
                          <label for="inputState">Home Address</label>
                          <input name="Saddress" type="text" class="form-control" id="Saddress">
                        </div>
                        <div class="form-group col-md-4">
                          <label for="inputZip">Religion</label>
                          <input name="Sreligion" type="text" class="form-control" id="Sreligion">
                        </div>
                      </div>
                  </div>
                  <div class="button-row d-flex mt-4">
                    <button class="btn btn-primary ml-auto js-btn-next btn-color" type="button" title="Next">Next</button>
                  </div>
                </div>
              </div>
              <!--single form parents-info-panel-->
              <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn">
                <h3 class="multisteps-form__title">Parent/Guardian Information</h3>
                <div class="multisteps-form__content">
                  <div class="row form-content">
                    <div class="form-row wid-100">
                      <div class="form-group col-md-6">
                        <label for="inputEmail4">1.Guardian First Name</label>
                        <input name="g1fname" type="text" class="form-control" id="G1fname">
                      </div>
                      <div class="form-group col-md-6">
                        <label for="inputPassword4">Second Name</label>
                        <input name="g1sname" type="text" class="form-control" id="G1sname">
                      </div>
                    </div>
                    <div class="form-row wid-100">
                      <div class="form-group col-md-6">
                        <label for="inputEmail4">Telephone</label>
                        <input name="g1tell" type="text" class="form-control" id="G1tell">
                      </div>
                      <div class="form-group col-md-6">
                        <label for="inputPassword4">Email</label>
                        <input name="g1email" type="text" class="form-control" id="G1email">
                      </div>
                    </div>
                    <div class="form-row wid-100">
                      <div class="form-group col-md-4">
                        <label for="inputCity">Occupation</label>
                        <input name="g1occupation" type="text" class="form-control" id="G1occupation">
                      </div>
                      <div class="form-group col-md-4">
                        <label for="inputState">Address</label>
                        <input name="g1address" type="text" class="form-control" id="G1address">
                      </div>
                      <div class="form-group col-md-4">
                        <label for="inputZip">Relationship</label>
                        <input name="g1rlship" type="text" class="form-control" id="G1rlship">
                      </div>
                    </div>
                    <div class="form-row wid-100">
                      <div class="form-group col-md-6">
                        <label for="inputEmail4">2.Guardian First Name</label>
                        <input name="g2fname" type="text" class="form-control" id="G2fname">
                      </div>
                      <div class="form-group col-md-6">
                        <label for="inputPassword4">Second Name</label>
                        <input name="g2sname" type="text" class="form-control" id="G2sname">
                      </div>
                    </div>
                    <div class="form-row wid-100">
                      <div class="form-group col-md-6">
                        <label for="inputEmail4">Telephone</label>
                        <input name="g2tell" type="text" class="form-control" id="G2tell">
                      </div>
                      <div class="form-group col-md-6">
                        <label for="inputPassword4">Email</label>
                        <input name="g2email" type="text" class="form-control" id="G2email">
                      </div>
                    </div>
                    <div class="form-row wid-100">
                      <div class="form-group col-md-4">
                        <label for="inputCity">Occupation</label>
                        <input name="g2occupation" type="text" class="form-control" id="G2occupation">
                      </div>
                      <div class="form-group col-md-4">
                        <label for="inputState">Address</label>
                        <input name="g2address" type="text" class="form-control" id="G2address">
                      </div>
                      <div class="form-group col-md-4">
                        <label for="inputZip">Relationship</label>
                        <input name="g2rlship" type="text" class="form-control" id="G2rlship">
                      </div>
                    </div>
                  </div>
                  <div class="button-row d-flex mt-4">
                    <button class="btn btn-primary js-btn-prev btn-color" type="button" title="Prev">Prev</button>
                    <button class="btn btn-primary ml-auto js-btn-next btn-color" type="button" title="Next">Next</button>
                  </div>
                </div>
              </div>
              <!--single form education-info-panel-->
              <div class="multisteps-form__panel shadow p-4 rounded bg-white " data-animation="scaleIn">
                <h3 class="multisteps-form__title">Education Information</h3>
                <div class="multisteps-form__content">
                <div class="row form-content">
                      <div class="form-row wid-100">
                        <div class="form-group col-md-5">
                          <label for="inputCity">Name of Previous School</label>
                          <input name="prevschool" type="text" class="form-control" id="Prevschoolname">
                        </div>
                        <div class="form-group col-md-5">
                          <label for="inputState">Examination Index Number </label>
                          <input name="indexnumber" type="text" class="form-control" id="Indexnumber">
                        </div>
                        <div class="form-group col-md-2">
                          <label for="inputZip">Year</label>
                          <input name="compyr" type="date" class="form-control" id="Compyr">
                        </div>
                      </div>
                      <div class="form-row wid-100">
                        <div class="col-md-5">
                          <div class="form-group">
                            <label for="">Apply for</label>
                            <div  class="btn-group btn-group-toggle wid-100" data-toggle="buttons">
                              <label class="btn btn-secondary active">
                                <input type="radio" name="options" onchange="hideCombination()" id="O-level" autocomplete="off" checked> O-level
                              </label>
                              <label class="btn btn-secondary ">
                                <input type="radio" name="options" onchange="showCombination()" id="A-level" autocomplete="off"> A-level
                              </label>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <label for="">Grade</label>
                            <select name="app_grade" id="Grade" class="custom-select">
                              <option selected>Example: Form I</option>
                              <option value="Form I">Form I</option>
                              <option value="Form II">Form II</option>
                              <option value="Form III">Form III</option>
                              <option value="Form IV">Form IV</option>
                              <option value="Form V">Form V</option>
                              <option value="Form VI">Form VI</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                          <label for="">Category</label>
                            <select name="app_category" id="Category" class="custom-select">
                              <option selected>Example: Art</option>
                              <option value="Science">Science</option>
                              <option value="Art">Art</option>
                              <option value="Business">Business</option>
                            </select>
                        </div>
                      </div>
                      <div class="form-row wid-100">
                        <div class="form-group col-md-5">
                          <label for="inputCity">Upload <span id="which-result"></span> Results</label>
                          <div class="input-group">
                            <div class="custom-file">
                              <input name="result_file" type="file" class="custom-file-input" id="inputGroupFile04">
                              <label class="custom-file-label" for="inputGroupFile04">Choose file</label>
                            </div>
                            <!-- <div class="input-group-append">
                              <button class="btn btn-outline-secondary" type="button">Button</button>
                            </div> -->
                          </div>
                        </div>
                        <div class="form-group col-md-4">
                          <label for="inputState">Division</label>
                            <select name="division" class="custom-select" id="Division">
                              <option selected>Example: Division I</option>
                              <option value="Division I">Division I</option>
                              <option value="Division II">Division II</option>
                              <option value="Division III">Division III</option>
                              <option value="Division IV">Division IV</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                          <label for="inputZip">Point</label>
                          <input name="oint" type="number" class="form-control" id="Point">
                        </div>
                      </div>
                    <!-- <div class="form-row wid-100">
                      <div class="btn-group btn-group-toggle" data-toggle="buttons">
                        <label class="btn btn-secondary active">
                          <input type="radio" name="options" id="option1" autocomplete="off" checked> Form I
                        </label>
                        <label class="btn btn-secondary">
                          <input type="radio" name="options" id="option2" autocomplete="off"> Form II
                        </label>
                        <label class="btn btn-secondary">
                          <input type="radio" name="options" id="option3" autocomplete="off"> Form III
                        </label>
                        <label class="btn btn-secondary">
                          <input type="radio" name="options" id="option3" autocomplete="off"> Form IV
                        </label>
                          <label class="btn btn-secondary">
                          <input type="radio" name="options" id="option3" autocomplete="off"> Form V
                        </label>
                        </label>
                          <label class="btn btn-secondary">
                          <input type="radio" name="options" id="option3" autocomplete="off"> Form VI
                        </label>
                      </div>
                    </div> -->
                    <div id="comb-select" class="combination wid-100" style="display:none">
                      <p>Choose Combination</p>
                      <div class="form-row wid-100">
                        <div class="custom-control custom-checkbox custom-control-inline">
                          <input type="checkbox" class="custom-control-input" id="customCheck1">
                          <label class="custom-control-label" for="customCheck1">HKL</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                          <input type="checkbox" class="custom-control-input" id="customCheck2">
                          <label class="custom-control-label" for="customCheck2">HGL</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                          <input type="checkbox" class="custom-control-input" id="customCheck3">
                          <label class="custom-control-label" for="customCheck3">HGE</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                          <input type="checkbox" class="custom-control-input" id="customCheck4">
                          <label class="custom-control-label" for="customCheck4">HGK</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                          <input type="checkbox" class="custom-control-input" id="customCheck5">
                          <label class="custom-control-label" for="customCheck5">PCM</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                          <input type="checkbox" class="custom-control-input" id="customCheck6">
                          <label class="custom-control-label" for="customCheck6">PCB</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                          <input type="checkbox" class="custom-control-input" id="customCheck7">
                          <label class="custom-control-label" for="customCheck7">PGM</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                          <input type="checkbox" class="custom-control-input" id="customCheck8">
                          <label class="custom-control-label" for="customCheck8">CBG</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                          <input type="checkbox" class="custom-control-input" id="customCheck9">
                          <label class="custom-control-label" for="customCheck9">ECA</label>
                        </div>
                      </div>  
                    </div>
                  </div>
                  <div id="tempo" class="button-row d-flex mt-4">
                    <button class="btn btn-primary js-btn-prev btn-color" type="button" title="Prev">Prev</button>
                    <button onclick="insertToPreview()" class="btn btn-primary ml-auto js-btn-next btn-color" type="button" title="Next">Next</button>
                  </div>
                </div>
              </div>
              <!--single form preview-panel-->
              <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn">
                <!-- <h3 class="multisteps-form__title">Preview Form</h3> -->
                <div class="multisteps-form__content">
                <div class="preview">
                  <div class="row prev-header">
                    <div class="col-md-10" style="text-align:center">
                      <h5>INSTITUTE OF EDUCATION AND TRAINING TANZANIA</h5>
                        <h6>FEZA BOYS SECONDARY SCHOOL</h6>
                        <h6>O-LEVEL APLLICATION FORM 2020</h6>
                        <P>S.L.P MBEZI, DAR ES SALAAM</P>
                    </div>
                    <div class="col-md-2">
                      <img src="images/logo.svg"  class="d-inline-block align-top logo-sm" alt="">
                    </div>
                  </div>
                  <hr>
                  <div class="row prev-personal">
                    <table class="table table-borderless table-sm ">
                      <thead class="thead-light">
                        <tr>
                          <th scope="col" colspan="3">PERSONAL INFORMATION</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td colspan="1">First Name: <span id="sfname"></span></td>
                          <td colspan="1">Second Name: <span id="ssname"></span></td>
                        </tr>
                        <tr>
                          <td>Email: <span id="semail"></span> </td>
                          <td>Telephone: <span id="stell"></span></td>
                          <td colspan="1">Home Address: <span id="saddress"></span></td>
                        </tr>
                        <tr>
                          <td >Dirt of Birth: <span id="sbirthdate"></span></td>
                          <td>Gender: <span id="sgender"></span></td>
                          <td colspan="1">Age: <span id="sage"></span></td>
                        </tr>
                        <tr>
                          <td >Nationality: <span id="snationality"></span></td>
                          <td>Religion: <span id="sreligion"></span></td>
                          <td colspan="1">Place of Birth: <span id="splacebirth"></span></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="row prev-parents">
                    <table class="table table-borderless table-sm wid-100">
                      <thead class="thead-light">
                        <tr>
                          <th scope="col" colspan="3">PARENT/GUARDIAN INFORMATION</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td colspan="1">1.Guardian First Name: <span id="g1fname"></span></td>
                          <td colspan="1">1.Second Name: <span id="g1sname"></span></td>
                        </tr>
                        <tr>
                          <td colspan="1">Email: <span id="g1email"></span> </td>
                          <td colspan="1">Telephone: <span id="g1tell"></span></td>
                        </tr>
                        <tr>
                          <td >Occupation:  <span id="g1occupation"></span></td>
                          <td>Address: <span id="g1address"></span></td>
                          <td colspan="1">Relationship <span id="g1rlship"></span></td>
                        </tr>
                        <tr>
                          <td colspan="2"></td>
                        </tr>
                        <tr>
                          <td colspan="1">2.Guardian First Name: <span id="g2fname"></span></td>
                          <td colspan="1">Second Name: <span id="g2sname"></span></td>
                        </tr>
                        <tr>
                          <td colspan="1">Email: <span id="g2email"></span> </td>
                          <td colspan="1">Telephone: <span id="g2tell"></span></td>
                        </tr>
                        <tr>
                          <td >Occupation:  <span id="g2occupation"></span></td>
                          <td>Address: <span id="g2address"></span></td>
                          <td colspan="1">Relationship <span id="g2rlship"></span></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="row prev-education">
                    <table class="table table-borderless table-sm ">
                      <thead class="thead-light">
                        <tr>
                          <th scope="col" colspan="3">EDUCATIONAL INFORMATION</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td colspan="1">Previous School Name: <span id="prevschoolname"></span></td>
                          <td >Index Number: <span id="indexnumber"></span></td>
                          <td >Completion Year: <span id="compyr"></span></td>
                        </tr>
                        <tr>
                          <td colspan="1">Applied For: <span>A-level</span> </td>
                          <td>Grade: <span id="grade" ></span></td>
                          <td >Category: <span id="category"></span></td>
                        </tr>
                        <tr>
                          <td colspan="1">Results Type: <span>SCEE</span> </td>
                          <td>Division: <span id="division"></span></td>
                          <td >Points: <span id="point"></span></td>
                        </tr>
                        <tr>
                          <td colspan="1">Combinations Selected: 
                            <ul>
                              <li>PCM</li>
                              <li>PGM</li>
                              <li>PCB</li>
                            </ul>
                          </td>
                          <td colspan="1">Attachments: 
                            <ul >
                              <li>CSEE Results</li>
                              <li>Passport Photo</li>
                              <li>Recommendatio Letter</li>
                            </ul>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="row prev-footer">
                    <p>feza boys seconady school ,Dar es Salaam</p>
                    <p>+255273643/06786543213 , fax 345-002</p>
                    <p>@faze schools</p>
                  </div>
                </div>
                  <div id="tempo" class="button-row d-flex mt-4">
                    <button class="btn btn-primary js-btn-prev btn-color" type="button" title="Prev">Prev</button>
                    <button class="btn btn-primary ml-auto js-btn-next btn-color" type="button" title="Next">Confirm</button>
                  </div>
                </div>
              </div>
              <!--single form payment-panel  -->
              <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn">
                <h3 class="multisteps-form__title">Application Payment</h3>
                <div class="multisteps-form__content">
                  <div class="pay">
                <div class="row mpesa">
                  <form class="form-inline">
                    <div class="form-group mb-2">
                      <label for="staticEmail2" class="sr-only">Email</label>
                      <input type="text" readonly class="form-control-plaintext" id="staticEmail2" value="M-pesa">
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                      <label for="inputPassword2" class="sr-only">Number</label>
                      <input type="number" class="form-control" id="inputPassword2" placeholder="Enter vodacom number">
                    </div>
                    <button  class="btn btn-primary mb-2">Proceed</button>
                  </form>
                </div>
                <div class="row halopesa">
                  <form class="form-inline">
                    <div class="form-group mb-2">
                      <label for="staticEmail2" class="sr-only">Email</label>
                      <input type="text" readonly class="form-control-plaintext" id="staticEmail2" value="Halo pesa">
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                      <label for="inputPassword2" class="sr-only">Password</label>
                      <input type="number" class="form-control" id="inputPassword2" placeholder="Enter Halotel number">
                    </div>
                    <button  class="btn btn-primary mb-2">Proceed</button>
                  </form>
                </div>
                <div class="row tigopesa">
                  <form class="form-inline">
                    <div class="form-group mb-2">
                      <label for="staticEmail2" class="sr-only">  </label>
                      <input type="text" readonly class="form-control-plaintext" id="staticEmail2" value="Tigo pesa">
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                      <label for="inputPassword2" class="sr-only">Password</label>
                      <input type="number" class="form-control" id="inputPassword2" placeholder="Enter Tigo number">
                    </div>
                    <button  class="btn btn-primary mb-2">Proceed</button>
                  </form>
                </div>
                </div>
                  <div class="button-row d-flex mt-4">
                    <button class="btn btn-primary btn-color js-btn-prev" type="button" title="Prev">Prev</button>
                    <button class="btn btn-success btn-color ml-auto" type="submit" title="Send">Submit</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- partial -->

@endsection
