@extends('layouts.mainlayout')
@section('content')
<div class="container-fluid">
<div class="container-fluid school-header">
    <div class="container ">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="/home">
                <img src="images/logo.svg"  class="d-inline-block align-top logo-sm" alt="">
            </a>
            <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent" style="justify-content:flex-end">
                <div class="home-navbar">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Contact Us</a>
                        </li>
                        <li class="nav-item">
                          <a href="/login">
                            <button  type="submit" class="btn btn-color btn-sm">Log in</button>
                          </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="container">
<div class="row home-mid">
     <div class="col srch">
        <div class="srch-txt">
          <h2>Search for school of your choice</h2>
          <h5>start now and find school of your choice</h5>
        </div>
        <div class="row srch-box">
            <div class="col-md-8 offset-md-2" style="padding-right:0px">
              <input type="email" class="form-control" placeholder="Search for school "  onkeyup="showHint(this.value)">
            </div>
         <div class="col-md-2" style="padding-left:0px">
            <button  type="submit" class="btn btn-color ">Search</button>
         </div>
        </div>
        <div class="row">
          <div class="col-md-5 offset-md-2 results-wrapper">
            <p id="results"></p>
         </div>
        </div>
    </div>
  </div>  
</div>
    <div class="row">

    </div>
</div>
@endsection
