<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'studentController@getStudents');
Route::get('/searchschool/{str}', 'SchoolController@searchSchool');
Route::get('/signup','studentController@openForm');
// Route::post('/store','studentController@saveStudent');
// Route::get('/school','RegionController@getRegion');

Route::get('/form','UserController@openForm');
Route::post('/fillform','UserController@fillForm');
Route::post('/addschool','SchoolController@create');
Route::get('/school/{id}','SchoolController@retrive');
Route::get('/login',function(){
    return view('login');
});
// Route::get('/form',function(){
//     return view('form');
// });
Route::get('/',function(){
    return view('home');
});
Route::get('/school',function(){
    return view('school');
});
Route::get('/admin',function(){
    return view('admin');
});

