<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Payment;

class PaymentController extends Controller
{
    public function create(Request $request){

        $payment = new Payment();

        $payment->status = $request->status;
        $payment->amount = $request->amount;
        $payment->application_id = $request->application_id;

        if($payment->save()) return response()->json(['message'=>'payment created']);
        else return response()->json(['error'=>'failed to create payment']);

    }

    public function update(Request $request,$id){

        $payment = Payment::find($id);

        $payment->status =$request->has('status')?$request->status:$payment->status;
        $payment->amount = $request->has('amount')?$request->amount:$payment->amount;
        $payment->application_id =$request->has('application_id')?$request->application_id:$payment->application_id;

        if($payment->save()) return $this->show($id);
        // if($payment->save()) return response()->json(['message'=>'payment updated']);
        else return response()->json(['error'=>'failed update payment']);

    }
    
    public function showAll(){

        $payment = Payment::all();

        return response()->json(['results'=>$payment]);
    }

    public function show($id){

        $payment = Payment::with('application')->find($id);

        return response()->json(['results'=>$payment]);
    } 

    public function delete(Request $request,$id){

        $payment = Payment::find($id);

        if($payment->delete())  return response()->json(['message'=>'payment deleted']);
    }
}
