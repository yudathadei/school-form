<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolCombinationsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'school_combinations';

    /**
     * Run the migrations.
     * @table school_combinations
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('combination_id');
            $table->unsignedInteger('school_id');

            $table->index(["school_id"], 'fk_school_combinations_schools1_idx');

            $table->index(["combination_id"], 'fk_school_combinations_combinations1_idx');
            $table->nullableTimestamps();


            $table->foreign('combination_id', 'fk_school_combinations_combinations1_idx')
                ->references('id')->on('combinations')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('school_id', 'fk_school_combinations_schools1_idx')
                ->references('id')->on('schools')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
