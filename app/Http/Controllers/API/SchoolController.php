<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\School;

class SchoolController extends Controller
{
    public function create(Request $request){

        $school = new School();

        $school->name = $request->name;
        $school->email = $request->email;
        $school->phone_number = $request->phone;
        $school->fax = $request->fax;
        $school->fee = $request->fee;
        $school->logo = $request->logo;
        $school->image = $request->image;
        $school->description = $request->description;
        $school->accepted_gender = $request->gender;
        $school->accomodation_type = $request->accomodation;
        $school->postal_address = $request->address;
        $school->requirement = "bado";
        $school->district_id = 2;
        $school->website = $request->web;

        if($school->save()) return view('home');

    }

    public function showAll(){

        $school = School::with('schoolCombinations.combination','district.region')->get();

        return response()->json(['results'=>$school]);
    }

    public function show($id){

        $school = school::with('schoolCombinations.combination','district.region')->find($id);
        

        return response()->json(['result'=>$school]);

    }

    public function update(Request $request,$id){

        $school = School::find($id);

        $school->name = $request->has('name')?$request->name :$school->name;
        $school->fee = $request->has('fee')?$request->fee :$school->fee;
        $school->email = $request->has('email')?$request->email :$school->email;
        $school->fax = $request->has('fax')?$request->fax :$school->fax;
        $school->phone_number = $request->has('phone_number')?$request->phone_number :$school->phone_number;
        $school->website = $request->has('website')?$request->website :$school->website;
        $school->postal_address = $request->has('postal_address')?$request->postal_address :$school->postal_address;
        $school->district_id = $request->has('district_id')?$request->district_id :$school->district_id;
        $school->description = $request->has('description')?$request->description :$school->description;
        $school->requirement = $request->has('requirement')?$request->requirement :$school->requirement;
        $school->accomodation_type = $request->has('accomodation_type')?$request->accomodation_type :$school->accomodation_type;
        $school->accepted_gender = $request->has('accepted_gender')?$request->accepted_gender:$school->accepted_gender;

        // if ($school->save()) return $this->show($id);
        if($school->save()) return response()->json(['message'=>'scholl updated']);
        else return response('failed to update the school');
    }

    public function delete($id){

        $school = school::find($id);

        if($school->delete()) return response()->json(['messsage'=>'school deleted']);
        else return response()->json(['error'=>'record not found']);

    }

}
