<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolCombination extends Model

{
    public function combination(){

        return $this->belongsTo('App\Combination')->select('id','name');
    }

    public function school(){

        return $this->belongsTo('App\School')->select('id','name');
    }


}
