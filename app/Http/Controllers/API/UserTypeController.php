<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\UserType;

class UserTypeController extends Controller
{
    public function test(){

        $data = UserType::with('users')->get();

        return response()->json(['result'=>$data]);
    }
    
}
