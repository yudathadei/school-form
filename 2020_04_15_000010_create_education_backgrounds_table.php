<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationBackgroundsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'education_backgrounds';

    /**
     * Run the migrations.
     * @table education_backgrounds
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('prev_school_name', 200)->nullable();
            $table->string('completion_year', 200)->nullable();
            $table->string('examination_number', 200)->nullable();
            $table->string('result_file', 250)->nullable();
            $table->string('division', 200)->nullable();
            $table->unsignedInteger('user_id');

            $table->index(["user_id"], 'fk_education_backgrounds_users1_idx');


            $table->foreign('user_id', 'fk_education_backgrounds_users1_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
