@extends('layouts.mainlayout')
@section('content')
<div class="container-fluid school-header">
    <div class="container ">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="/home">
                <img src="images/logo.svg"  class="d-inline-block align-top logo-sm" alt="">
            </a>
            <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent" style="justify-content:flex-end">
                <div class="home-navbar">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Contact Us</a>
                        </li>
                        <li class="nav-item">
                          <a href="/login">
                            <button  type="submit" class="btn btn-color btn-sm">Log in</button>
                          </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="container">
    <div class="row admin-general">
<form action="addschool" method="POST">
@csrf
  <h2 class="btn-color">GENERAL SCHOOL INFROMATION</h2>
    <div class="form-group ">
        <label for="inputAddress">Enter School Name</label>
        <input name="name" type="text" class="form-control" id="inputAddress">
    </div>
  <div class="form-row ">
    <div class="form-group col-md-6">
      <label for="inputEmail4">School Email</label>
      <input name="email" type="email" class="form-control" id="inputEmail4" placeholder="Email">
    </div>
    <div class="form-group col-md-6">
      <label for="inputPassword4">School Website/Blog</label>
      <input name="web" type="text" class="form-control" id="inputPassword4" >
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputCity">Upload School Logo</label>
      <div class="custom-file">
        <input name="logo" type="file" class="custom-file-input" id="customFile">
        <label class="custom-file-label" for="customFile">Choose file</label>
      </div>
    </div>
    <div class="form-group col-md-6">
      <label for="inputState">Upload School Image</label>
      <div class="custom-file">
        <input name="image" type="file" class="custom-file-input" id="customFile">
        <label class="custom-file-label" for="customFile">Choose file</label>
      </div>
    </div>
  </div>
  <div class="form-row">
      <div class="form-group col-md-4">
        <label for="inputAddress2">1.Telephone</label>
        <input name="phone" type="text" class="form-control" >
      </div>
      <div class="form-group col-md-4">
        <label for="inputAddress2">2.Telephone</label>
        <input type="text" class="form-control" >
      </div>
      <div class="form-group col-md-4">
        <label for="inputAddress2">3.Telephone</label>
        <input type="text" class="form-control" >
      </div>
  </div>
  <div class="form-row">
      <div class="form-group col-md-4">
        <label for="inputAddress2">Post Address</label>
        <input name="address" type="text" class="form-control" >
      </div>
      <div class="form-group col-md-4">
        <label for="inputAddress2">Fax</label>
        <input name="fax" type="text" class="form-control" >
      </div>
      <div class="form-group col-md-4">
        <label for="inputAddress2">District</label>
        <input name="district"  type="text" class="form-control" >
      </div>
  </div>
  <div class="form-row">
      <div class="form-group col-md-4">
        <label for="inputAddress2">School Fee</label>
        <input name="fee" type="text" class="form-control" >
      </div>
      <div class="form-group col-md-4">
        <label for="inputAddress2">Accomodation Type</label>
        <input name="accomodation" type="text" class="form-control" >
      </div>
      <div class="form-group col-md-4">
        <label for="inputAddress2">Accepted Gender</label>
        <input name="gender" type="text" class="form-control" >
      </div>
  </div>
  <div class="form-group">
    <label for="exampleFormControlTextarea1">School Description</label>
    <textarea name="description" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>
  <h2 class="btn-color">INTERVIEW INFROMATION</h2>
  <div class="form-row">
    <div class="form-group col-md-6">
        <label for="inputAddress2">Application Deadline</label>
        <input name="app_deadline" type="date" class="form-control" >
    </div>
    <div class="form-group col-md-6">
        <label for="inputAddress2">Application Fee</label>
        <input name="app_fee" type="text" class="form-control" >
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-4">
        <label for="inputAddress2">Enterview Date</label>
        <input name="ent_date" type="date" class="form-control" >
    </div>
    <div class="form-group col-md-4">
        <label for="inputAddress2">Enterview Venue</label>
        <input name="ent_venue" type="text" class="form-control" >
    </div>
    <div class="form-group col-md-4">
        <label for="inputAddress2">Enterview Time</label>
        <input name="ent_time" type="text" class="form-control" >
    </div>
  </div>
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Enterview Notice</label>
    <textarea name="ent_notice" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>
<div class="low-btn">
    <button type="submit" class="btn btn-primary btn-color">Submit</button>
</div>
</form>
</div>
</div>
</div>
@endsection