<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="bootstrap-4.4.1-dist/css/bootstrap.min.css" >
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
<link rel="stylesheet" href="css/sf.css">
</head>
<body>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-5 signup_left" style="background-image:url('images/student1.svg')" >
      <div class="signup_form">
        <h2>Create account to join ShuleForm</h2>   
        <h4>Join ShuleForm as </h4>
        <div class="tab-wrapper">
          <ul class="nav nav-tabs" id="myTab" role="tablist" style="width: fit-content">
            <li class="nav-item">
              <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Student</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Parent</a>
            </li>
          </ul>
        </div>
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
            <form action="signup">
              <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" placeholder="Email">
              </div>
              <div class="form-group">
                <label for="mobile">Mobile</label>
                <input type="tell" class="form-control" placeholder="Mobile">
              </div>
              <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" placeholder="Password">
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="fname">First Name</label>
                  <input type="text" class="form-control" id="fname" placeholder="First Name">
                </div>
                <div class="form-group col-md-6">
                  <label for="lname">Last Name</label>
                  <input type="text" class="form-control" id="lname" placeholder="Last Name">
                </div>
              </div>
              <button type="submit" class="btn btn-primary">Sign in</button>
              <p>Already have an account <a href="/login"> Log in</a> </p>
            </form>
          </div>
          <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <form action="signup">
              <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" placeholder="Email">
              </div>
              <div class="form-group">
                <label for="mobile">Mobile</label>
                <input type="tell" class="form-control" placeholder="Mobile">
              </div>
              <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" placeholder="Password">
              </div>
              <button type="submit" class="btn btn-primary">Sign in</button>
              <p>Already have an account <a href=""> Log in</a> </p>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-7 signup_right" >
      <div> 
        <div class="row right_top">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <img class="logo" src="images/Logo.svg" alt="logo">
            </div>
            <div class="col-md-4">
                <img class="top_blob" src="images/blob2.svg" alt="gray blob top right">
            </div>
        </div>
      </div>
      <div class="row right_mid">
          <h1>it's time to find you a school</h1>
          <h6>create free account now to find Perfect school of your choice</h6>
      </div>
      <div class="row right_low"  >
        <img src="images/right.svg" alt="" class="righ_img">
      </div>
    </div>
  </div>
</div>

    

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="js/sf.js"></script>
</body>
</html>