function signupForm(){
    document.getElementById("signup").style.display="block";
}

function showHint(str) {
    if (str.length == 0) {
      document.getElementById("results").innerHTML = "";
      return;
    } else {
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          var results = JSON.parse(this.responseText); 
          // console.log(results);
          var search_res = ''; 
          for (let index = 0; index < results.length; index++) {
            var district = results[index].district; 
            search_res = search_res+ '<a  href="/school/'+results[index].id+'" class="result-row">'+results[index].name+' - '+district.name+' , '+district.region.name+'</a>';

            // search_res = search_res+ '<a  href='+results[index].name+' class="result-row">'+results[index].name+' - '+district.name+' , '+district.region.name+'</a>';
          }
          document.getElementById("results").innerHTML = search_res;
        }
      };
    xmlhttp.open("GET", "searchschool/" + str, true);
      xmlhttp.send();
    }
  }
  
    //check if there is input
    // if yes validate the response
    // chunk result by json
    // manupulate response
    // return response

    // xml get data
    // xmlsend data

    // stollen wizard from javascript

    //DOM elements
const DOMstrings = {
  stepsBtnClass: 'multisteps-form__progress-btn',
  stepsBtns: document.querySelectorAll(`.multisteps-form__progress-btn`),
  stepsBar: document.querySelector('.multisteps-form__progress'),
  stepsForm: document.querySelector('.multisteps-form__form'),
  stepsFormTextareas: document.querySelectorAll('.multisteps-form__textarea'),
  stepFormPanelClass: 'multisteps-form__panel',
  stepFormPanels: document.querySelectorAll('.multisteps-form__panel'),
  stepPrevBtnClass: 'js-btn-prev',
  stepNextBtnClass: 'js-btn-next' };


//remove class from a set of items
const removeClasses = (elemSet, className) => {

  elemSet.forEach(elem => {

    elem.classList.remove(className);

  });

};

//return exect parent node of the element
const findParent = (elem, parentClass) => {

  let currentNode = elem;

  while (!currentNode.classList.contains(parentClass)) {
    currentNode = currentNode.parentNode;
  }

  return currentNode;

};

//get active button step number
const getActiveStep = elem => {
  return Array.from(DOMstrings.stepsBtns).indexOf(elem);
};

//set all steps before clicked (and clicked too) to active
const setActiveStep = activeStepNum => {

  //remove active state from all the state
  removeClasses(DOMstrings.stepsBtns, 'js-active');

  //set picked items to active
  DOMstrings.stepsBtns.forEach((elem, index) => {

    if (index <= activeStepNum) {
      elem.classList.add('js-active');
    }

  });
};

//get active panel
const getActivePanel = () => {

  let activePanel;

  DOMstrings.stepFormPanels.forEach(elem => {

    if (elem.classList.contains('js-active')) {

      activePanel = elem;

    }

  });

  return activePanel;

};

//open active panel (and close unactive panels)
const setActivePanel = activePanelNum => {

  //remove active class from all the panels
  removeClasses(DOMstrings.stepFormPanels, 'js-active');

  //show active panel
  DOMstrings.stepFormPanels.forEach((elem, index) => {
    if (index === activePanelNum) {

      elem.classList.add('js-active');

      setFormHeight(elem);

    }
  });

};

//set form height equal to current panel height
const formHeight = activePanel => {

  const activePanelHeight = activePanel.offsetHeight;

  DOMstrings.stepsForm.style.height = `${activePanelHeight}px`;

};

const setFormHeight = () => {
  const activePanel = getActivePanel();

  formHeight(activePanel);
};

//STEPS BAR CLICK FUNCTION
DOMstrings.stepsBar.addEventListener('click', e => {

  //check if click target is a step button
  const eventTarget = e.target;

  if (!eventTarget.classList.contains(`${DOMstrings.stepsBtnClass}`)) {
    return;
  }

  //get active button step number
  const activeStep = getActiveStep(eventTarget);

  //set all steps before clicked (and clicked too) to active
  setActiveStep(activeStep);

  //open active panel
  setActivePanel(activeStep);
});

//PREV/NEXT BTNS CLICK
DOMstrings.stepsForm.addEventListener('click', e => {

  const eventTarget = e.target;

  //check if we clicked on `PREV` or NEXT` buttons
  if (!(eventTarget.classList.contains(`${DOMstrings.stepPrevBtnClass}`) || eventTarget.classList.contains(`${DOMstrings.stepNextBtnClass}`)))
  {
    return;
  }

  //find active panel
  const activePanel = findParent(eventTarget, `${DOMstrings.stepFormPanelClass}`);

  let activePanelNum = Array.from(DOMstrings.stepFormPanels).indexOf(activePanel);

  //set active step and active panel onclick
  if (eventTarget.classList.contains(`${DOMstrings.stepPrevBtnClass}`)) {
    activePanelNum--;

  } else {

    activePanelNum++;

  }

  setActiveStep(activePanelNum);
  setActivePanel(activePanelNum);

});

//SETTING PROPER FORM HEIGHT ONLOAD
window.addEventListener('load', setFormHeight, false);

//SETTING PROPER FORM HEIGHT ONRESIZE
window.addEventListener('resize', setFormHeight, false);

//changing animation via animation select !!!YOU DON'T NEED THIS CODE (if you want to change animation type, just change form panels data-attr)

const setAnimationType = newType => {
  DOMstrings.stepFormPanels.forEach(elem => {
    elem.dataset.animation = newType;
  });
};

//selector onchange - changing animation
const animationSelect = document.querySelector('.pick-animation__select');

animationSelect.addEventListener('change', () => {
  const newAnimationType = animationSelect.value;

  setAnimationType(newAnimationType);
});


// education information pannel controls
 function showCombination(){
    document.getElementById('which-result').innerHTML='CSEE';
    document.getElementById('comb-select').style.display= "block";
    document.getElementById('division').disabled = false;
    document.getElementById('point').disabled = false;
    document.getElementById('catg').disabled = false;

 }
 function hideCombination(){
  document.getElementById('which-result').innerHTML='Final';
  document.getElementById('comb-select').style.display= "none";
  document.getElementById('Division').disabled = true;
  document.getElementById('Point').disabled = true;
  document.getElementById('Category').disabled = true;

}

function assignToStorage(ids){
  for(var i=0; i< ids.length; i++){
    var item = ids[i];
     window.localStorage.setItem(item,document.getElementById(item).value);
  }
}
function printValues(ids){
  for(var i=0; i< ids.length;i++){
    var item=ids[i].toLowerCase();
    document.getElementById(item).innerHTML = window.localStorage.getItem(ids[i]);
  }
}
function insertToPreview(){
  var personids= ["Sfname","Ssname","Semail","Sbirthdate","Sgender","Sage","Snationality","Splacebirth","Stell","Saddress","Sreligion","G1fname","G1sname","G1email","G1tell","G1address","G1occupation","G1rlship","G2fname","G2sname","G2email","G2tell","G2address","G2occupation","G2rlship","Prevschoolname","Compyr","Indexnumber","Grade","Category","Division","Point"];
  assignToStorage(personids);
  printValues(personids);
}

