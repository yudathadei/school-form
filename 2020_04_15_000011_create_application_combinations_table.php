<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationCombinationsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'application_combinations';

    /**
     * Run the migrations.
     * @table application_combinations
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('application_id')->nullable();
            $table->integer('priority')->nullable();
            $table->unsignedInteger('combination_id');

            $table->index(["combination_id"], 'fk_application_combinations_combinations1_idx');

            $table->index(["application_id"], 'fk_application_combinations_applications1_idx');
            $table->nullableTimestamps();


            $table->foreign('application_id', 'fk_application_combinations_applications1_idx')
                ->references('id')->on('applications')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('combination_id', 'fk_application_combinations_combinations1_idx')
                ->references('id')->on('combinations')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
