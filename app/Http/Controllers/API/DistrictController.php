<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\District;

class DistrictController extends Controller
{
    public function show($id){

        $district = District::with('schools')->find($id);

        return response()->json(['results'=>$district]);
    }
}
