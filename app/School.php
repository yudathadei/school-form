<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    public function schoolCombinations(){

        return $this->hasMany('App\SchoolCombination');
    }

    public function district(){

        return $this->belongsTo('App\District');

    }
    public function interviews(){

        return $this->hasMany('App\Interview');
    }
}
