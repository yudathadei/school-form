<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'applications';

    /**
     * Run the migrations.
     * @table applications
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('status', 200);
            $table->unsignedInteger('user_id');
            $table->enum('education_level', ['o_level', 'a_level'])->nullable()->default(null);
            $table->string('grade', 45)->nullable();
            $table->string('category', 45)->nullable();

            $table->index(["user_id"], 'fk_applications_Users1_idx');
            $table->nullableTimestamps();


            $table->foreign('user_id', 'fk_applications_Users1_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
