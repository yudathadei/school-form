<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\School;

class SchoolController extends Controller
{
    public function searchSchool($str){

        $results = School::with('district.region')->where('name','like','%'.$str.'%')->get();

        echo \json_encode($results);

    }
    
    public function create(Request $request){


        $interview=app('App\Http\Controllers\InterviewController');
        $interview->create($request);

        $school = new School();

        $school->name = $request->name;
        $school->email = $request->email;
        $school->phone_number = $request->phone;
        $school->fax = $request->fax;
        $school->fee = $request->fee;
        $school->logo = $request->logo;
        $school->image = $request->image;
        $school->description = $request->description;
        $school->accepted_gender = $request->gender;
        $school->accomodation_type = $request->accomodation;
        $school->postal_address = $request->address;
        $school->requirement = "bado";
        $school->district_id = 2;
        $school->website = $request->web;

        if($school->save()) return redirect('/');

    }

    public function retrive($id){

        $school = School::with('interviews')->find($id);

        foreach($school->interviews as $interview){
            if($interview->school_id == $id){
            break;
            }
        }
        return view('school',['school'=>$school,'interview'=>$interview]);
    }
}
